# **Day 1**
|||
| ---|---|
|**Part 1**||
| Context of application | 00:19:12 |
| Architecture | 00:35:43 |
| Versions|01:00:54|
| Dependencies|01:07:10|
| Packages|01:10:45|
| Training Center|01:13:51|
|**Part 2**||
|Intro into Content Script|00:03:00|
|Create Content Script|00:08:22|
|Content Script IDE|00:14:11|
|Autocomplete|00:16:08|
|Documentation|00:31:13|
|Snippets|00:34:44|
|Co-editing|00:40:18|
|Compare with previous version|00:42:03|
|Impersonate users|00:59:00|
|**Part 3**||
|Variables|00:04:09|
|List & Map|00:08:16|
|Control operators|00:11:18|
|Control structures (while, for, etc)|00:11:43|
|Switch example|00:13:50|
|Reserved Keywords|00:15:23|
|Operators Cheat sheet|00:17:45|
|Debug|00:33:58|
|Closure|00:49:07|
# Day 2
|||
| ---|---|
|**Part 1**||
|TBA||
