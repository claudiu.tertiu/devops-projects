# CHEATSHEET
## COMMANDS
1. #### $ ```kubectl run [POD_NAME] --image=[IMAGE_NAME]``` // create a pod from an image
2. #### $ ```kubectl create deployment [POD_NAME] --image=[IMAGE_NAME] ``` // create a deployment using imperative command
3. #### $ ```kubectl create -f [YAML_FILE]``` // create pods using a yaml file
4. #### $ ```kubectl describe pod [POD_NAME]``` // gives you a more detailed pod description
5. #### $ ```kubectl apply -f [YAML_FILE]``` // same as create if you want to create a new resource
6. #### $ ```kubectl edit pod [POD_NAME]``` // opens the yaml config for that pod in vim
7. #### $ ```kubectl get pods --selector=[LABEL_NAME]=[VALUE_FOR_LABEL]``` //
8. #### $ ```kubectl get pods -o wide ``` // displays a list of all pods in the current namespace, along with other information
9. #### $ ``` kubectl delete pod [POD_NAME] --grace-period=0 --force``` // doesn't wait 30 seconds to gracefully flush the workload;
10. #### $ ``` kubectl config current-context``` // get current context
11. #### $ ``` kubectl config use-context [CONTEXT_NAME]``` // switch to another context
12. #### $ ``` kubectl config set-context [CONTEXT_NAME] --cluster=minikube --user=[USER_NAME]``` // set a context entry in `kubeconfig` for `user`
13. #### $ ``` kubectl rollout undo deployment/[POD_NAME] -to-revision=1``` // rolls back to the first version
14. #### $ ``` kubectl scale deployment [POD_NAME] --replicas=[NO_OF_REPLICAS]``` // sets the number of replicas for autoscaling to it
15. #### $ ``` kubectl autoscale deployment/[POD_NAME] --cpu-percent=[PERCENTAGE_NO] --min=[MIN_REPLICAS] --max=[MAX_REPLICAS]``` // if the average (all cpu utilizations summed up) threshold is over a percetange it will add another pod
16. #### $ ``` kubectl create deployment/[POD_NAME] --image=[IMAGE_NAME] --replicas=[NO_OF_REPLICAS]``` // create a deployment pod with nginx image and 3 replicas
17. #### $ ``` kubectl set image deployment/[POD_NAME] [IMAGE_NAME]=[IMAGE]:[VERSION] --record``` // records the changes in the rollout history after setting an image, instead of going over yaml file manually
18. #### $ ``` kubectl create configmap [CONFIG_MAP_NAME] --from-literal=[KEY]=[VALUE]``` // create configmap from literal values
19. #### $ ``` kubectl create configmap [CONFIG_MAP_NAME] --from-env-file=[FILE_NAME.env]``` // single file with environment variables
20. #### $ ``` kubectl create configmap [CONFIG_MAP_NAME] --from-file=[FILE_NAME]``` // file or directory
21. #### $ ``` kubectl describe quota --namespace=[NAMESPACE]``` // shows the memory usage, CPU details of a namespace and no of pods
22. #### $ ``` kubectl run [POD_NAME] --image=[IMAGE_NAME] -o yaml --dry-run=client > pod.yaml``` // creates a pod.yaml file for the config
23. #### $ ``` kubectl create ns [NAMESPACE]``` // creates a namespace 
24. #### $ ``` kubectl config set-context --curent --namespace=[NAMESPACE]``` // modify the current namespace 
25. #### $ ``` kubectl label [TYPE_OF_POD] [LABEL]``` // add a label to a specific pod 
26. #### $ ``` minikube delete --all``` // delete all the profiles 
27. #### $ ``` minikube start --nodes [NO_OF_NODES]``` // start a cluster with multiple nodes
28. #### $ ``` kubectl label [TYPE_OF_POD] [POD_NAME] [LABEL] --overwrite``` // in case you want to modify an already existing label
29. #### $ ``` kubectl taint [RESOURCE_TYPE] [RESOURCE_NAME] [TAINT_KEY]-``` // remove that specific taint 
30. #### $ ``` kubectl create service [SERVICE_TYPE] [SERVICE_NAME] --tcp=80:80``` // create a service for networking with tcp protocol mapped to 80:80
31. #### $ ``` kubectl run nginx --image=nginx --port=80 --expose``` // create a pod and expose it inside cluster
32. #### $ ``` ``` // 
20. #### $ ``` ``` // 


## TIPS & TRICKS
1. #### ```Pod required properties are: ``` 
        apiVersion:
        kind: 
        metadata: 
        spec:
2. #### ``` Table with kinds & versions of pods in K8s```
    | Kind | Version |
    | ----------- | ----------- |
    | Pod | v1 |
    | Service | v1 | 
    | ReplicaSet | apps/v1 | 
    | Deployment | apps/v1 | 
3. #### ``` Kubernetes documentation: https://kubernetes.io/docs.```
4. #### ``` Rolling update means that whenever you update a replicaSet, it will update the nodes, one by one.```
5. #### ``` Rolling back can be done using by either updating the template or using rolling back to a revision.```
5. #### ``` ConfigMap config file.```
    apiVersion:
    data:
        db: staging # secret added
        username: jdoe # secret added
    kind: ConfigMap
    metadata: 
        name: db-config

5. #### ``` Node Affinity: Attract Pods to a node as soft or hard requirement. ```
5. #### ``` Node Taint: Allow a node to repel a set of Pods.```
5. #### ``` Node Tolerations: Applied to Pods to allow scheduling them to nodes with a specific taint.```
5. #### ``` Service types = ClusterIP / NodePort / LoadBalancer / ExternalName```
5. #### ``````
5. #### ``````
5. #### ``````