# CHEATSHEET
## KEYBOARD SHORTCUTS
1. #### ```WIN+E``` // open file explorer;
2. #### ```WIN+Esc``` // open Start Menu;
3. #### ```WIN+S``` // open Search;
4. #### ```WIN+K``` // open Connect Settings, for connecting to bluetooth headset;
5. #### ```WIN+CTRL+D``` // create New Virtual Desktop, helpfull for being more organised;
6. #### ```WIN+P``` // project settings, for multiple screens or video projector;
7. #### ```WIN+SHIFT+S``` // Snip Tool, for screnshots;
7. #### ```ALT+SHIFT``` // change Keyboard Language;

## TIPS & TRICKS
1. #### ```[INSERT_A_PIECE_OF_ADVICE]```