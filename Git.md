# CHEATSHEET
## COMMANDS
1. #### ```git pull --prune``` // remove all remote references that no longer exist on the remote;
2. #### ```git pull --dry-run``` // show the action that would be completed without actually making changes to your repo;
3. #### ```git status``` // check the local repository status (files added, not included etc.);
4. #### ```git status -s``` // a short version of the command above (A - added, M - modified, D - deleted etc.);
5. #### ```git add [FILENAME]``` // add a file to the current commit;
6. #### ```git commit -m "[MESSAGE]"``` // add a commit to the local repo with a message attached;
7. #### ```git commit -m "[MESSAGE]"``` // same as the one from above but with the add part included;
8. #### ```git commit --allow-empty -m "[EMPTY_COMMIT_MESSAGES]"``` // it creates a commit with no file added (normally you'd get an error) if not using that option;

9. #### ```git diff --staged``` // checks the differences between the staging part & remote;
10. #### ```git branch --show-current``` // see all branches;
11. #### ```git checkout -b [BRANCH_NAME]``` // create a new branch;
12. #### ```git log``` // check the commits history;
13. #### ```git branch --set-upstream-to=[REPO_NAME]/[BRANCH_NAME]``` // check the commits history;
14. #### ```git stash``` // saves local modifs and reverts the working directory to match the HEAD commit;
15. #### ```git stash show``` // display all stashes;
16. #### ```git stash drop``` // remove the first stash from the list of stashes;
17. #### ```git merge --abort``` // in case you've made a pull that resulted with a conflict merge, get back to normal with this command;
18. #### ```git push -f origin [COMMIT_HASH]:[BRANCH_NAME]``` // in case you've made a pull that resulted with a conflict merge, get back to normal with this command;

## TIPS & TRICKS
1. #### ```If you want to use the dif tool provided by git and you'd like to configure it, use: git config difftool```
2. #### ```Create .gitkeep file in every empty folder you would like to have on repository. Without any file in it, git won't add that empty folder to the remote.```