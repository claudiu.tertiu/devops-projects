# CHEATSHEET
## KEYBOARD SHORTCUTS
1. #### ```CTRL+SHIFT+X ``` // opens the plugins window;
2. #### ```CTRL+P``` // go to file;
3. #### ```ALT+CLICK``` // multi-cursor selection;
4. #### ```CTRL+K R``` // reveal selected file in explorer;
5. #### ```CTRL+SHIFT+H``` // replace in files;
6. #### ```CTRL+K CTRL+S``` // keyboard shortcuts from VSCode;
7. #### ```CTRL+K Z``` // open Zen Mode, is better for presentations;
8. #### ```CTRL+K CTRL+X``` // trims whitespaces;
9. #### ```CTRL+K V``` // open Markdown preview to the side;
10. #### ```SHIFT+ESCAPE``` // show Terminal Window;
11. #### ```ALT+T``` // open a new Terminal Window;
11. #### ```CTRL+"+" ","``` // enter in Settings Mode;

## TIPS & TRICKS
1. #### ```If you are in Debug Mode and you want to do some modifications to the Debug configs, press CTRL+SHIFT+D and press on: "create a launch.json file"```.    [VS Code link here](https://code.visualstudio.com/docs/editor/debugging)
2. #### ``` ```