# CHEATSHEET
## COMMANDS
1. #### ```asCSNode([INSERT_ID])``` // retrieve a Content Script node using its ID;
2. #### ```redirect "${url}[INSERT_PATH]``` // redirects to an URL from servers domain;
3. #### ```json([INSERT_MAP])``` // creates a json using key:value pairs from map;
4. #### ```[]``` // ;
5. #### ```[]``` // ;
6. #### ```[]``` // ;
7. #### ```[]``` // ;
8. #### ```[]``` // ;
9. #### ```[]``` // ;
10. #### ```[]``` // ;
11. #### ```[]``` // ;
12. #### ```[]``` // ;

## TIPS & TRICKS
1. #### ```A closure is an anonymous block of code. In Groovy, it is an instance of the Closure class. Closures can take 0 or more parameters and always return a value. Additionally, a closure may access surrounding variables outside its scope and use them — along with its local variables — during execution```

